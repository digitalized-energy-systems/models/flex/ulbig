import numpy as np

# The device parameters from OpenTUMFlex are modified/transformed to fit into the unified model formulation of Ulbig.


def modify_params_Ulbig(OpenTUM_res, t_fac):
    ulbig_devices = {}
    fac = int(24 / t_fac)
    for device in OpenTUM_res['devices']:
        # extract data of chosen device
        params = OpenTUM_res['devices'][device]
        opt_op_plan = OpenTUM_res['optplan']

        # create modified parameter dict in model fitting format
        params_mod = {'opt_plan': float(0), 'u_gen,max': float(0),
                      'u_gen,min': float(0), 'u_load,max': float(0),
                      'u_load,min': float(0), 'eta_el': float(0),
                      'xi_max': float(0), 'xi_min': float(0),
                      'w_max': float(0), 'w_min': float(0),
                      'nue': float(0), 'cap': float(0),
                      'x_max': float(0), 'x_min': float(0),
                      'ava': float(0), 'SOC': float(0)}

        # fill modified parameter dict
        # Battery
        if device == 'bat':
            params_mod['opt_plan'] = opt_op_plan['bat_output_power'] - opt_op_plan['bat_input_power']
            params_mod['u_gen,max'] = params['maxpow']
            params_mod['u_load,max'] = params['maxpow']
            params_mod['u_gen,min'] = params['minpow']
            params_mod['u_load,min'] = params['minpow']
            params_mod['eta_el'] = params['eta']
            params_mod['xi_max'] = np.zeros(fac)
            params_mod['xi_min'] = 0
            params_mod['w_max'] = params_mod['xi_max']
            params_mod['w_min'] = params_mod['xi_min']
            params_mod['nue'] = 0
            params_mod['cap'] = params['stocap']
            params_mod['x_max'] = 100
            params_mod['x_min'] = 0
            params_mod['ava'] = [np.float64(1)] * fac
            params_mod['SOC'] = np.insert(opt_op_plan['bat_SOC'], 0, params['initSOC'])[0:fac]
            ulbig_devices[device] = params_mod

        # Combined Heat and Power
        if device == 'chp':
            params_mod['opt_plan'] = opt_op_plan['CHP_elec_pow']
            params_mod['u_gen,max'] = params['maxpow']
            params_mod['u_load,max'] = 0
            params_mod['u_gen,min'] = params['minpow']
            params_mod['u_load,min'] = 0
            params_mod['eta_el'] = params['eta'][0]
            params_mod['xi_max'] = np.full(fac, params['maxpow'] / params['eta'][0])
            params_mod['xi_min'] = params['minpow'] / params['eta'][0]
            params_mod['w_max'] = params_mod['xi_max']
            params_mod['w_min'] = params_mod['xi_min']
            params_mod['nue'] = 0
            params_mod['cap'] = 0
            params_mod['x_max'] = 0
            params_mod['x_min'] = 0
            params_mod['ava'] = [np.float64(1)] * fac
            params_mod['SOC'] = [np.float64(0)] * fac
            ulbig_devices[device] = params_mod

        # Photovoltaik
        if device == 'pv':
            params_mod['opt_plan'] = OpenTUM_res['fcst']['solar_power'] * np.array(params['maxpow'])
            params_mod['u_gen,max'] = params['maxpow']
            params_mod['u_load,max'] = 0
            params_mod['u_gen,min'] = 0
            params_mod['u_load,min'] = 0
            params_mod['eta_el'] = 1
            params_mod['xi_max'] = OpenTUM_res['fcst']['solar_power'] * np.array(params['maxpow'])
            params_mod['xi_min'] = 0
            params_mod['w_max'] = params_mod['xi_max']
            params_mod['w_min'] = params_mod['xi_min']
            params_mod['nue'] = 0
            params_mod['cap'] = 0
            params_mod['x_max'] = 0
            params_mod['x_min'] = 0
            params_mod['ava'] = [np.float64(1)] * fac
            params_mod['SOC'] = [np.float64(0)] * fac
            ulbig_devices[device] = params_mod

        # Electric Vehicle
        if device == 'ev':
            params_mod['opt_plan'] = opt_op_plan['EV_power']
            params_mod['u_gen,max'] = 0
            params_mod['u_load,max'] = np.array([params['maxpow']] * fac)
            params_mod['u_gen,min'] = 0
            params_mod['u_load,min'] = params['minpow']
            params_mod['eta_el'] = np.array([params['eta']] * fac)
            params_mod['xi_max'] = np.zeros(fac)
            params_mod['xi_min'] = np.zeros(fac)
            params_mod['w_max'] = params_mod['xi_max']
            params_mod['w_min'] = params_mod['xi_min']
            params_mod['nue'] = 0
            params_mod['cap'] = params['stocap']
            params_mod['x_max'] = 100
            params_mod['x_min'] = 0
            params_mod['ava'] = params['aval']
            params_mod['SOC'] = np.insert(OpenTUM_res['optplan']['EV_SOC'], 0, params['initSOC'][0])[0:fac]
            ulbig_devices[device] = params_mod

        # Heat Pump
        if device == 'hp':
            params_mod['opt_plan'] = opt_op_plan['HP_elec_power']
            params_mod['u_gen,max'] = 0
            params_mod['u_load,max'] = OpenTUM_res['optplan']['HP_ele_run']
            params_mod['u_gen,min'] = 0
            params_mod['u_load,min'] = params['minpow']
            params_mod['eta_el'] = OpenTUM_res['optplan']['HP_COP']
            params_mod['xi_max'] = np.zeros(fac)
            params_mod['xi_min'] = -OpenTUM_res['optplan']['HP_ele_run'] * OpenTUM_res['optplan']['HP_COP']
            params_mod['w_max'] = params_mod['xi_max']
            params_mod['w_min'] = params_mod['xi_min']
            params_mod['nue'] = 0
            params_mod['cap'] = 0
            params_mod['x_max'] = 0
            params_mod['x_min'] = 0
            params_mod['ava'] = [np.float64(1)] * fac
            params_mod['SOC'] = [np.float64(0)] * fac
            ulbig_devices[device] = params_mod

    return ulbig_devices
