# Flexibility-Trinity after [Ulbig and Andersson (2012)](https://ieeexplore.ieee.org/document/6344676)
 
Implementation of the flexibility model presented by [Ulbig and Andersson (2012)](https://ieeexplore.ieee.org/document/6344676). 

This program allows the flexbility calculation of elecrical storage-, load- and generation-devices and is designed to fit into [unified_flex_scenario](https://gitlab.com/digitalized-energy-systems/scenarios/unified_flex_scenario).
 
