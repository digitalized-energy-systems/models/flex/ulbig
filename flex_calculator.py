import numpy as np

'''Flexibility is calculated depending on the optimal operation plans of
all electric devices and their unique parameters.
For that and depending on certain characteristics the devices are categorized
into 3 types: generators, loads and storages (all electrical).'''


def calc_flex(ulbig_devices, sto_con, t_fac):
    ulbig_flex = {}
    # Loop through all electric devices
    for device in ulbig_devices:
        # Retrieve variables
        opt_plan = ulbig_devices[device]['opt_plan']
        u_gen_max = ulbig_devices[device]['u_gen,max']
        u_gen_min = ulbig_devices[device]['u_gen,min']
        u_load_max = ulbig_devices[device]['u_load,max']
        u_load_min = ulbig_devices[device]['u_load,min']
        eta_el = ulbig_devices[device]['eta_el']
        xi_max = ulbig_devices[device]['xi_max']
        xi_min = ulbig_devices[device]['xi_min']
        w_max = ulbig_devices[device]['w_max']
        w_min = ulbig_devices[device]['w_min']
        nue = ulbig_devices[device]['nue']
        cap = ulbig_devices[device]['cap']
        x_max = ulbig_devices[device]['x_max']
        x_min = ulbig_devices[device]['x_min']
        SOC = ulbig_devices[device]['SOC']

        # Flex calculation for generation devices
        if np.mean(np.array(u_load_max)) == 0:
            # Generator
            d_type = 'generator'
            u_gen_fea_max = np.zeros(len(opt_plan), dtype=float)
            u_gen_fea_min = np.zeros(len(opt_plan), dtype=float)
            pi_gen_pos = np.zeros(len(opt_plan), dtype=float)
            pi_gen_neg = np.zeros(len(opt_plan), dtype=float)

            for t_step, opt_power in enumerate(opt_plan):
                # Calculate maximal pos/neg feasible operation points
                u_gen_feas_max = eta_el * (xi_max[t_step] - w_min - nue - cap
                                           * (x_min - SOC[t_step]) / 100 / t_fac)
                u_gen_feas_max = min(u_gen_feas_max, u_gen_max)
                u_gen_feas_min = eta_el * (xi_min - w_max[t_step] - nue - cap
                                           * (x_max - SOC[t_step]) / 100 / t_fac)
                u_gen_feas_min = max(u_gen_feas_min, u_gen_min)

                # Calculate maximal pos/neg feasible deviation from opt. op
                pi_plus_gen = u_gen_feas_max - opt_power
                pi_minus_gen = u_gen_feas_min - opt_power

                # SOC check: if storage SOC bounds are violated -> flexibility correction
                if cap > 0 and sto_con:
                    max_SOC = max(SOC[t_step + 1:97])
                    min_SOC = min(SOC[t_step + 1:97])
                    if (pi_plus_gen * t_fac) / cap * 100 > min_SOC * eta_el:
                        pi_plus_gen = (min_SOC / 100 * cap) / t_fac * eta_el
                        u_gen_feas_max = pi_plus_gen + opt_power
                    if (-pi_minus_gen * t_fac) / cap * 100 > (100 - max_SOC) / eta_el:
                        pi_minus_gen = -(100 - max_SOC) / 100 * cap / t_fac / eta_el
                        u_gen_feas_min = pi_minus_gen + opt_power

                # Save results
                u_gen_fea_max[t_step] = u_gen_feas_max
                u_gen_fea_min[t_step] = u_gen_feas_min
                pi_gen_pos[t_step] = pi_plus_gen
                pi_gen_neg[t_step] = pi_minus_gen

            # Save results of generation device
            flex = {'device_type': d_type,
                    'opt_plan': opt_plan,
                    'u_gen_fea_max': u_gen_fea_max,
                    'u_gen_fea_min': u_gen_fea_min,
                    'pi_gen_pos': pi_gen_pos,
                    'pi_gen_neg': pi_gen_neg}
            ulbig_flex[device] = flex

        # Flex calculation for load devices
        elif np.mean(np.array(u_gen_max)) == 0:
            # Load
            d_type = 'load'
            u_load_fea_max = np.zeros(len(opt_plan), dtype=float)
            u_load_fea_min = np.zeros(len(opt_plan), dtype=float)
            pi_load_pos = np.zeros(len(opt_plan), dtype=float)
            pi_load_neg = np.zeros(len(opt_plan), dtype=float)

            for t_step, opt_power in enumerate(opt_plan):
                # Calculate maximal pos/neg feasible operation points
                u_load_feas_max = 1 / eta_el[t_step] * (-xi_min[t_step] + w_max[t_step] + nue
                                                        + cap * (x_max - SOC[t_step]) / 100 / t_fac)
                u_load_feas_max = min(u_load_feas_max, u_load_max[t_step])
                u_load_feas_min = 1 / eta_el[t_step] * (-xi_max[t_step] + w_min[t_step] + nue
                                                        + cap * (x_min - SOC[t_step]) / 100 / t_fac)
                u_load_feas_min = max(u_load_feas_min, u_load_min)

                # Calculate maximal pos/neg feasible deviation from opt. op
                pi_plus_load = u_load_feas_max - opt_power
                pi_minus_load = u_load_feas_min - opt_power

                # SOC check: if storage SOC bounds are violated -> flexibility correction
                if cap > 0 and sto_con:
                    max_SOC = max(SOC[t_step + 1:97])
                    min_SOC = min(SOC[t_step + 1:97])
                    if (pi_plus_load * t_fac) / cap * 100 > (100 - max_SOC) / eta_el[t_step]:
                        pi_plus_load = (100 - max_SOC) / 100 * cap / t_fac / eta_el[t_step]
                        u_load_feas_max = pi_plus_load + opt_power
                    if (-pi_minus_load * t_fac) / cap * 100 > min_SOC * eta_el[t_step]:
                        pi_minus_load = -min_SOC / 100 * cap / t_fac * eta_el[t_step]
                        u_load_feas_min = pi_minus_load + opt_power

                # Save results
                u_load_fea_max[t_step] = u_load_feas_max
                u_load_fea_min[t_step] = u_load_feas_min
                pi_load_pos[t_step] = pi_plus_load
                pi_load_neg[t_step] = pi_minus_load

            # Save results of load device
            flex = {'device_type': d_type,
                    'opt_plan': opt_plan,
                    'u_load_fea_max': u_load_fea_max,
                    'u_load_fea_min': u_load_fea_min,
                    'pi_load_pos': pi_load_pos,
                    'pi_load_neg': pi_load_neg}
            ulbig_flex[device] = flex

        # Flex calculation for storage devices
        else:
            # Storage
            d_type = 'storage'
            u_gen_fea_max = np.zeros(len(opt_plan), dtype=float)
            u_load_fea_max = np.zeros(len(opt_plan), dtype=float)
            pi_gen_pos = np.zeros(len(opt_plan), dtype=float)
            pi_load_pos = np.zeros(len(opt_plan), dtype=float)

            for t_step, opt_power in enumerate(opt_plan):
                # Calculate maximal pos/neg feasible operation points
                u_gen_feas_max = eta_el * (eta_el * u_load_min + xi_max[t_step] - w_min
                                           - nue - cap * (x_min - SOC[t_step]) / 100 / t_fac)
                u_gen_feas_max = min(u_gen_feas_max, u_gen_max)
                u_load_feas_max = 1 / eta_el * (1 / eta_el * u_gen_min - xi_min + w_max[t_step]
                                                + nue + cap * (x_max - SOC[t_step]) / 100 / t_fac)
                u_load_feas_max = min(u_load_feas_max, u_load_max)

                # Calculate maximal pos/neg feasible deviation from opt. op
                pi_plus_gen = u_gen_feas_max - opt_power
                pi_plus_load = u_load_feas_max + opt_power

                # SOC check: if storage SOC bounds are violated -> flexibility correction
                if cap > 0 and sto_con:
                    max_SOC = max(SOC[t_step + 1:97])
                    min_SOC = min(SOC[t_step + 1:97])
                    if (pi_plus_gen * t_fac) / cap * 100 > min_SOC * eta_el:
                        pi_plus_gen = min_SOC / 100 * cap / t_fac * eta_el
                        u_gen_feas_max = pi_plus_gen + opt_power
                    if (pi_plus_load * t_fac) / cap * 100 > (100 - max_SOC) / eta_el:
                        pi_plus_load = (100 - max_SOC) / 100 * cap / t_fac / eta_el
                        u_load_feas_max = pi_plus_load - opt_power

                # Save results
                u_gen_fea_max[t_step] = u_gen_feas_max
                u_load_fea_max[t_step] = u_load_feas_max
                pi_gen_pos[t_step] = pi_plus_gen
                pi_load_pos[t_step] = pi_plus_load

            # Save results of load device
            flex = {'device_type': d_type,
                    'opt_plan': opt_plan,
                    'u_gen_fea_max': u_gen_fea_max,
                    'u_load_fea_max': u_load_fea_max,
                    'pi_gen_pos': pi_gen_pos,
                    'pi_load_pos': pi_load_pos}
            ulbig_flex[device] = flex

    return ulbig_flex
