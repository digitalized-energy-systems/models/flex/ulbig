from .modify_parameters_Ulbig import modify_params_Ulbig
from .flex_calculator import calc_flex

'''Flexibility model after the formulation of Ulbig et. al (2012).
Calculates electrical flexibility out of operation plans and devices parameters
for single discretized time steps.'''


def flex_Ulbig(OpenTUM_res, flex_calc):
    # Time factor for conversion calculations
    t_fac = OpenTUM_res['time_data']['t_inval'] / 60
    
    # Turn storage controll on/off
    sto_con = False
    if flex_calc:
        # Modify parameters to fit with ulbig model
        ulbig_devices = modify_params_Ulbig(OpenTUM_res, t_fac)

        # Calculate flexibility
        ulbig_flex = calc_flex(ulbig_devices, sto_con, t_fac)

        # Store results
        ulbig_res = {'ulbig_devices': ulbig_devices, 'ulbig_flex': ulbig_flex}
    else:
        ulbig_res = 'Ulbig flexibility calculation is turned off.'
    return ulbig_res
